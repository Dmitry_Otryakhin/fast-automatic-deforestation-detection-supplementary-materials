import os
from pathlib import Path

import environ

from .exceptions import ImproperlyConfigured

config = None


def get_config():
    if config is None:
        from_envs()
    return config


def set_config(value):
    global config
    config = value
    return config


def from_envs():
    """Read configuration and store it."""
    return set_config(GeoImgConfig.from_environ(os.environ))


def _parse_dir(path_str: str) -> Path:
    dir_path = Path(path_str).expanduser().resolve()
    return dir_path


def apply_envs():
    """Set eodag envs from config, raise if missed."""
    assert config
    if not (config.peps.user and config.peps.password):
        raise ImproperlyConfigured(
            "Please set GI_PEPS_USER and " "GI_PEPS_PASSWORD environment variables."
        )
    os.environ["EODAG__PEPS__AUTH__CREDENTIALS__USERNAME"] = config.peps.user
    os.environ["EODAG__PEPS__AUTH__CREDENTIALS__PASSWORD"] = config.peps.password


@environ.config(prefix="GI")
class GeoImgConfig:

    img_dir = environ.var(
        "~/geoimages/sentinel/",
        converter=_parse_dir,
        help="Directory with images and RGB composites",
    )
    search_params_dir = environ.var(
        "~/geoimages/search/",
        converter=_parse_dir,
        help="Directory with JSON files with search params.",
    )

    @environ.config
    class Peps:
        user = environ.var(
            "",
            help="Username to access PEPS service",
        )
        password = environ.var(
            "",
            help="Password to access PEPS service",
        )

    @environ.config
    class Compression:
        """Options related to compression to pass to rasterio library."""

        enabled = environ.bool_var(True)
        compress = environ.var("DEFLATE", str)
        predictor = environ.var(2, int)
        zlevel = environ.var(9, int)

    peps = environ.group(Peps)
    compression = environ.group(Compression)
