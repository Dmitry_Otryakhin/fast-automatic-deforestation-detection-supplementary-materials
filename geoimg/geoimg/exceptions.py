class BaseGeoImgException(Exception):
    pass


class ImproperlyConfigured(BaseGeoImgException):
    """Raised when configuration is wrong."""


class IncorrectInput(BaseGeoImgException):
    """Raised when input is wrong."""
