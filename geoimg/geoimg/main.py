import json
from pathlib import Path

import pydantic

from . import conf, rgb, source
from .exceptions import IncorrectInput


class Main:
    def __init__(self):
        self.config = conf.from_envs()
        self.config.img_dir.mkdir(parents=True, exist_ok=True)
        self.config.search_params_dir.mkdir(parents=True, exist_ok=True)

    def list_products(self):
        for product_name in rgb.list_products():
            yield product_name

    def make_rgb(self, remake_existed=False):
        rgb.make_rgbs(skip_existing=not remake_existed)

    def search(self, search_params_path: Path, download=False, with_rgb=False):
        if download:
            conf.apply_envs()
        path_to_search_params = self._parse_params_path(search_params_path)
        search_results = {}
        for params_path, search_params in path_to_search_params.items():
            search_results[params_path] = source.search(
                search_params, download=download, with_rgb=with_rgb
            )
        return search_results

    def _parse_params_path(self, params_path: Path):
        if not params_path.exists():
            raise IncorrectInput(f'Path "{params_path}" does not exist')

        files = []
        if params_path.is_file():
            files.append(params_path)
        elif params_path.is_dir():
            for path in params_path.iterdir():
                files.append(path)
            if not files:
                raise IncorrectInput(f'Directory "{params_path}" is empty')

        path_to_search_params = {}
        for path in files:
            search_params = self._parse_params_file(path)
            path_to_search_params[path] = search_params
        return path_to_search_params

    def _parse_params_file(self, params_path: Path):
        try:
            with open(params_path) as params_file:
                _search_params = json.loads(params_file.read())
        except OSError as e:
            raise IncorrectInput(f"Cannot open file with search params: {repr(e)}")
        except json.JSONDecodeError as e:
            raise IncorrectInput(
                f"Cannot read json from file with search params: {repr(e)}"
            )
        try:
            search_params = source.SearchParams.parse_obj(_search_params)
        except pydantic.ValidationError as e:
            raise IncorrectInput(
                f"Incorrect search params in file {params_path}: {repr(e)}"
            )
        return search_params
