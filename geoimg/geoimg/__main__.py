from pathlib import Path

import click

from . import conf, exceptions
from .main import Main


@click.group()
def cli():
    pass


@cli.command()
def list():
    """
    List downloaded products.
    """
    app = Main()
    for image_id in app.list_products():
        click.echo(image_id)


@cli.command()
@click.option("-r", "--remake", is_flag=True, help="Rewrite existing RGB files")
def rgb(remake):
    """Make RGB composites for each image in IMG_DIR."""
    app = Main()
    app.make_rgb(remake_existed=remake)


_search_params_help = "Path to file with search params or directory with such files"
_no_rgb_help = "Do not RGB-composite after download."


@cli.command()
@click.option("-p", "--params-path", default="", help=_search_params_help)
@click.option(
    "-d", "--download", is_flag=True, help="Download found images to IMG_DIR."
)
@click.option("--no-rgb", is_flag=True, help=_no_rgb_help)
def search(params_path, download, no_rgb):
    """Search products using search params."""
    if not params_path:
        params_path = str(conf.get_config().search_params_dir)
    return _search(params_path, download, with_rgb=not no_rgb)


@cli.command()
@click.option("-p", "--params-path", default="", help=_search_params_help)
@click.option("--no-rgb", is_flag=True, help=_no_rgb_help)
def download(params_path, no_rgb):
    """Search and download products using search params."""
    return _search(params_path, download=True, with_rgb=not no_rgb)


def _search(params_path, download, with_rgb):
    app = Main()
    if not params_path:
        params_path = conf.get_config().search_params_dir
    params_path = Path(params_path)
    try:
        search_results = app.search(params_path, download=download, with_rgb=with_rgb)
    except (exceptions.ImproperlyConfigured, exceptions.IncorrectInput) as e:
        click.echo(f"{e.__class__.__name__}: {e}", err=True)

    for path, images in search_results.items():
        click.echo(path.name)
        for product in images:
            click.echo(product.properties["title"])


if __name__ == "__main__":
    cli()
