import os
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

import rasterio

from . import conf
from .logger import log, logger


def make_rgbs(skip_existing=True, product_names=None):
    if product_names is None:
        product_names = list(list_products())
    futures = []
    processed = 0
    errors = []

    with ThreadPoolExecutor() as executor:
        for product_name in product_names:
            future = executor.submit(
                make_rgb,
                product_name=product_name,
                skip_existing=skip_existing,
            )
        futures.append(future)
    for future in futures:
        try:
            future.result()
        except Exception as e:
            if errors:
                logger.exception("Error in making rgb:", exc_info=e)
            errors.append(e)
        else:
            processed += 1
    log(f"{processed} of {len(product_names)} images processed.")
    if errors:
        raise errors[0]


def make_rgb(product_name, skip_existing=False):
    rgb_path = _get_rgb_path(product_name)
    if skip_existing and rgb_path.exists():
        log(f"RGB for {product_name} already exists:\n" f"  {rgb_path}")
        return None
    return _make_rgb(product_name)


def list_products():
    for product_dir in conf.get_config().img_dir.iterdir():
        yield product_dir.name


def _get_product_dir(product_name: str) -> Path:
    return conf.get_config().img_dir / product_name


def _get_rgb_path(product_name: str) -> Path:
    return _get_product_dir(product_name) / f"{product_name}_rgb.tif"


def _get_tmp_rgb_path(product_name: str) -> Path:
    return _get_product_dir(product_name) / f"{product_name}_rgb_uncompressed.tif"


def _make_rgb(product_name: str):
    log(f"Creating RGB for {product_name}")

    product_dir = _get_product_dir(product_name)
    bands_dirs = list(product_dir.glob("**/IMG_DATA"))
    assert len(bands_dirs) == 1, bands_dirs
    bands_dir = bands_dirs[0]

    rgb_paths = {
        "red": bands_dir.glob("*_B04.jp2"),
        "green": bands_dir.glob("*_B03.jp2"),
        "blue": bands_dir.glob("*_B02.jp2"),
    }
    for key, path_gen in rgb_paths.items():
        matches = list(path_gen)
        assert len(matches) == 1, matches
        rgb_paths[key] = matches[0]

    tmp_file = _get_tmp_rgb_path(product_name)
    out_file = _get_rgb_path(product_name)

    blue = rasterio.open(rgb_paths["blue"])
    green = rasterio.open(rgb_paths["green"])
    red = rasterio.open(rgb_paths["red"])
    with rasterio.open(
        tmp_file,
        "w",
        driver="GTiff",
        width=blue.width,
        height=blue.height,
        count=3,
        crs=blue.crs,
        transform=blue.transform,
        dtype=blue.dtypes[0],
    ) as rgb:
        rgb.write(red.read(1), 1)
        rgb.write(green.read(1), 2)
        rgb.write(blue.read(1), 3)

    config = conf.get_config()
    if config.compression.enabled:
        source = str(tmp_file.absolute())
        target = str(out_file.absolute())
        options = config.compression
        cmd = (
            f"gdal_translate "
            f"-co driver=GTIFF -co compress={options.compress} "
            f"-co predictor={options.predictor} -co zlevel={options.zlevel} "
            f"{source} {target}"
        )
        exit_code = os.system(cmd)
        if exit_code == 0:
            os.remove(tmp_file)
        else:
            logger.error(f"Error while compressing {out_file}")
            os.rename(tmp_file, out_file)
    else:
        os.rename(tmp_file, out_file)
    log(f"  {out_file}")
    return out_file
