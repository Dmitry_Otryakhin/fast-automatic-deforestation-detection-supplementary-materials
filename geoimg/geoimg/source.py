import datetime
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

from eodag import EODataAccessGateway
from pydantic import BaseModel

from . import conf, rgb
from .logger import log, logger


class Footprint(BaseModel):
    lonmin: int
    latmin: int
    lonmax: int
    latmax: int


class SearchParams(BaseModel):
    start: datetime.date
    end: datetime.date
    geom: Footprint
    cloudCover: int


def search(search_params, *, download=False, with_rgb=False):
    search_results = _search(search_params)
    if download:
        with ThreadPoolExecutor() as executor:
            for path in _download(search_results):
                if with_rgb:
                    executor.submit(rgb.make_rgb, product_name=path.name)
    return search_results


def _search(search_params):
    dag = EODataAccessGateway()
    product_type = "S2_MSI_L1C"

    start = search_params.start.isoformat()
    end = search_params.end.isoformat()
    search_results, found_nb = dag.search(
        productType=product_type,
        geom=search_params.geom.dict(),
        start=start,
        end=end,
        cloudCover=search_params.cloudCover,
    )
    return search_results


def _download(products):
    all(p.properties["storageStatus"] == "ONLINE" for p in products)

    existed_products = set(rgb.list_products())
    new_products = []
    for product in products:
        product_name = _get_product_name(product)
        if product_name not in existed_products:
            new_products.append(product)
        else:
            log(f"Already have {product_name}")

    prefix = "file:"
    for product in new_products:
        try:
            product_path_str = product.download()
        except Exception as e:
            logger.exception(repr(e))
            continue
        if product_path_str.startswith(prefix):
            product_path_str = product_path_str.replace(prefix, "")
        tmp_path = Path(product_path_str).parent
        assert tmp_path.exists(), tmp_path
        path = tmp_path.rename(conf.get_config().img_dir / tmp_path.name)
        log("Downloaded as: {}".format(path))
        yield path


def _get_product_name(product):
    return product.properties["title"]
