import os
from pathlib import Path

import pytest
from click.testing import CliRunner

from geoimg.__main__ import cli


@pytest.fixture
def runner():
    return CliRunner()


def test_exit_code():
    exit_code = os.system("python -m geoimg")
    assert exit_code == 0


@pytest.mark.parametrize("help_command", ["--help", ""])
def test_help(help_command, runner):
    result = runner.invoke(cli, help_command)
    for keyword in ["Commands", "rgb", "search", "download"]:
        assert keyword in result.output


def test_list(runner, img_dir, image_ids):
    result = runner.invoke(cli, "list")
    assert result.exit_code == 0
    assert (set(image_ids)) == set(s for s in result.output.split("\n") if s)


@pytest.mark.parametrize("rgb_command", ["rgb", "rgb --remake", "rgb -r"])
def test_rgb(rgb_command, runner, mocker):
    fake_make_rgb = mocker.patch("geoimg.main.Main.make_rgb")

    result = runner.invoke(cli, rgb_command)

    assert result.exit_code == 0
    do_remake = not (rgb_command == "rgb")
    fake_make_rgb.assert_called_once_with(remake_existed=do_remake)


@pytest.mark.parametrize(
    "cmd_args",
    [
        ["search", "~/geoimages/search/", False, True],
        ["search -p .", ".", False, True],
        ["search -d", "~/geoimages/search/", True, True],
        ["search --no-rgb", "~/geoimages/search", False, False],
    ],
)
def test_search(cmd_args, runner, mocker):
    fake_search = mocker.patch("geoimg.main.Main.search")
    command, *expected_args = cmd_args

    result = runner.invoke(cli, command)

    assert result.exit_code == 0
    path, download, with_rgb = expected_args
    fake_search.assert_called_once()
    args, kwargs = fake_search.call_args_list[0]
    assert len(args) == 1
    actual_path = args[0]
    assert (
        Path(actual_path)
        .expanduser()
        .resolve()
        .samefile(Path(path).expanduser().resolve())
    )
    assert kwargs["download"] == download
    assert kwargs["with_rgb"] == with_rgb


@pytest.mark.parametrize(
    "cmd_args",
    [
        ["download", "~/geoimages/search/", True],
        ["download -p .", ".", True],
        ["download --no-rgb", "~/geoimages/search", False],
    ],
)
def test_download(cmd_args, runner, mocker):
    fake_search = mocker.patch("geoimg.main.Main.search")
    command, *expected_args = cmd_args

    result = runner.invoke(cli, command)

    assert result.exit_code == 0
    path, with_rgb = expected_args
    fake_search.assert_called_once()
    args, kwargs = fake_search.call_args_list[0]
    assert len(args) == 1
    actual_path = args[0]
    assert (
        Path(actual_path)
        .expanduser()
        .resolve()
        .samefile(Path(path).expanduser().resolve())
    )
    assert kwargs["download"] is True
    assert kwargs["with_rgb"] == with_rgb
