import shutil
import tempfile
from pathlib import Path

import pytest


@pytest.fixture
def image_ids():
    """Identifiers of some Sentinel images."""
    return [
        "S2A_MSIL1C_20210112T104411_N0209_R008_T31TCH_20210112T125127",
        "S2A_MSIL1C_20210102T104441_N0209_R008_T31TCJ_20210102T125215",
        "S2B_MSIL1C_20210110T105329_N0209_R051_T31TDJ_20210110T130049",
    ]


@pytest.fixture
def tmp_dir():
    with tempfile.TemporaryDirectory() as tmpdirname:
        yield Path(tmpdirname)


@pytest.fixture
def empty_img_dir(monkeypatch):
    with tempfile.TemporaryDirectory(prefix="img_dir-") as tmpdirname:
        images_dir = Path(tmpdirname) / "sentinel"
        images_dir.mkdir()
        monkeypatch.setenv("GI_IMG_DIR", str(images_dir))
        yield images_dir


@pytest.fixture
def img_dir(empty_img_dir, image_ids):
    """Mock of directory with downloaded source images."""
    for image_id in image_ids:
        img_dir = empty_img_dir / image_id
        img_dir.mkdir()


@pytest.fixture
def testdata_dir() -> Path:
    return Path(__file__).parent / "data"


@pytest.fixture
def rasters_data_dir(testdata_dir) -> Path:
    return testdata_dir / "sentinel"


@pytest.fixture
def img_dir_with_rasters(monkeypatch, image_ids, rasters_data_dir):
    with tempfile.TemporaryDirectory(prefix="img_dir-") as tmp_dir_name:
        images_dir = Path(tmp_dir_name) / "sentinel"
        shutil.copytree(rasters_data_dir, images_dir)
        monkeypatch.setenv("GI_IMG_DIR", str(images_dir))

        yield images_dir


@pytest.fixture
def peps_credentials(monkeypatch):
    user, password = "test_user", "test_password"
    monkeypatch.setenv("GI_PEPS_USER", user)
    monkeypatch.setenv("GI_PEPS_PASSWORD", password)
    return user, password
