import os
import pickle
import shutil
from argparse import Namespace
from pathlib import Path

import pytest
from eodag import EODataAccessGateway

from geoimg import exceptions
from geoimg.main import Main


def test_list(img_dir, image_ids):
    products = Main().list_products()
    assert set(image_ids) == set(products)


@pytest.mark.parametrize("remake", ["remake", "keep_existing"])
def test_make_rgb(remake, img_dir_with_rasters):
    remake = remake == "remake"

    Main().make_rgb(remake_existed=remake)

    image_dirs = list(img_dir_with_rasters.iterdir())
    assert len(image_dirs) == 1
    image_dir = image_dirs[0]
    result, created = None, None
    files = [path for path in image_dir.iterdir() if path.is_file()]
    assert len(files) == 1
    result = files[0]
    assert result.suffix == ".tif"
    stat = result.stat()
    created = stat.st_ctime
    assert created == stat.st_mtime

    Main().make_rgb(remake_existed=remake)

    new_stat = result.stat()
    if remake:
        assert new_stat.st_ctime > created
    else:
        assert new_stat.st_ctime == created


def _get_image_id(product):
    return product.properties["title"]


@pytest.fixture
def dag_result(testdata_dir, tmp_dir, rasters_data_dir):
    result_path = testdata_dir / "search_result.pkl"
    with open(result_path, "rb") as f:
        dag_result = pickle.load(f)
    products, _ = dag_result
    for product in products:
        image_id = _get_image_id(product)
        make_rasters = product is products[0]

        def _download(image_id=image_id, make_rasters=make_rasters):
            product_path = tmp_dir / image_id / f"{image_id}.SAFE/"
            product_path.mkdir(parents=True)
            if make_rasters:
                shutil.copytree(rasters_data_dir, tmp_dir, dirs_exist_ok=True)
            return "file:" + str(product_path)

        product.download = _download

    return dag_result


@pytest.fixture
def search_params():
    return dict(
        productType="S2_MSI_L1C",
        geom={"lonmin": 1, "latmin": 43, "lonmax": 2, "latmax": 44},
        start="2021-01-01",
        end="2021-01-15",
        cloudCover=90,
    )


@pytest.fixture
def search_params_dir(testdata_dir) -> Path:
    return testdata_dir / "search"


@pytest.fixture
def mocked_search(mocker, dag_result):
    return mocker.patch(
        "eodag.EODataAccessGateway.search",
        return_value=dag_result,
    )


@pytest.fixture
def search_session(search_params, search_params_dir, dag_result, mocked_search):
    return Namespace(
        search_params=search_params,
        search_params_dir=search_params_dir,
        dag_result=dag_result,
        mocked_search=mocked_search,
        product_with_rasters=dag_result[0][0],
    )


def test_search(search_session):
    result = Main().search(search_session.search_params_dir)
    search_session.mocked_search.assert_called_once_with(**search_session.search_params)
    assert result == {
        search_session.search_params_dir / "search.json": search_session.dag_result[0]
    }


def _make_dag_result(mocker, search_params_dir, testdata_dir):
    result_path = testdata_dir / "search_result.pkl"
    mocked_search = mocker.spy(
        EODataAccessGateway,
        "search",
    )

    Main().search(search_params_dir)

    print(f"{mocked_search.spy_return=}")
    with open(result_path, "wb") as f:
        pickle.dump(mocked_search.spy_return, f)


def test_download_check_envs(monkeypatch, search_session, empty_img_dir):
    with pytest.raises(exceptions.ImproperlyConfigured):
        Main().search(search_session.search_params_dir, download=True)
    monkeypatch.setenv("EODAG__PEPS__AUTH__CREDENTIALS__USERNAME", "user")
    monkeypatch.setenv("EODAG__PEPS__AUTH__CREDENTIALS__PASSWORD", "")
    with pytest.raises(exceptions.ImproperlyConfigured):
        Main().search(search_session.search_params_dir, download=True)


@pytest.mark.parametrize("make_rgb", ["with_rgb", "no_rgb"])
def test_download_new(make_rgb, peps_credentials, search_session, empty_img_dir):
    app = Main()
    assert app.config.peps.user, app.config.peps.password == peps_credentials
    with_rgb = make_rgb == "with_rgb"
    app.search(search_session.search_params_dir, download=True, with_rgb=with_rgb)
    assert (
        os.getenv("EODAG__PEPS__AUTH__CREDENTIALS__USERNAME"),
        os.getenv("EODAG__PEPS__AUTH__CREDENTIALS__PASSWORD"),
    ) == peps_credentials
    search_session.mocked_search.assert_called_once_with(**search_session.search_params)
    paths = [path for path in empty_img_dir.iterdir()]
    products = search_session.dag_result[0]
    assert len(paths) == len(products)
    assert set(p.name for p in paths) == set(_get_image_id(p) for p in products)
    for image_path in paths:
        image_id = image_path.name
        unpacked_path = image_path / f"{image_id}.SAFE"
        assert unpacked_path.exists()
        assert unpacked_path.is_dir()

    image_id = _get_image_id(search_session.product_with_rasters)
    rgb_path = empty_img_dir / image_id / f"{image_id}_rgb.tif"
    if with_rgb:
        assert rgb_path.exists()
    else:
        assert not rgb_path.exists()


def test_download_existed(peps_credentials, search_session, empty_img_dir):
    products = search_session.dag_result[0]
    for image_id in map(_get_image_id, products):
        (empty_img_dir / image_id).mkdir()
    Main().search(search_session.search_params_dir, download=True)
    search_session.mocked_search.assert_called_once_with(**search_session.search_params)
    for image_dir in empty_img_dir.iterdir():
        assert len(list(image_dir.iterdir())) == 0
