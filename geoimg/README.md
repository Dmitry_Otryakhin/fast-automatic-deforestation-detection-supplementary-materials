# GeoImg

GeoImg is a CLI-tool that can download Sentinel 2 images and make RGB composites.


## Getting Started
All shell commands presented here starts from project root (directory with this file).


### Prerequisites

* Ubuntu or other Linux distribution
* Python (expected versions from 3.6 to 3.8)


### Installing
* Clone repository using Git.
* No installation required, package can be run with `python -m` syntax. However, the package uses some external libraries, so we should prepare environment.
  Create virtualenv for app's dependencies using `pipenv`.
  If you have Python 3.8 best way is

    ```
    cd geoimg  # (directory with this README file, grandparent of __main__.py file)
    pip install pipenv
    pipenv shell
    pipenv install --deploy
    ```
  If you do not have python 3.8:

    ```
    cd geoimg
    pip install pipenv
    pipenv shell
    pipenv install--skip-lock
    ```


### Configuration
  Application read settings from environment variables:


  Available configuration variables are:

  - `GI_PEPS_USER`: psername to access PEPS service (required for download)
  - `GI_PEPS_PASSWORD`: password to access PEPS service (required for download)
  - `GI_IMG_DIR`: directory to download images and create RGB composites (default ~/geoimages/sentinel/)
  - `GI_SEARCH_PARAMS_DIR`: direstory with json files with search params (default ~/geoimages/search/). 
  Each file will be used by `search` and `download` commands to filter images
  on PEPS service (result is union of search by each parameters object in each file). 
  See [example of search params file](./examples/search.json).


  To set values one can use

  ```bash
  pipenv shell
  export GI_IMG_DIR=~/geoimages/sentinel/
  ```
  or just store values in [.env file](https://pipenv.pypa.io/en/latest/advanced/#automatic-loading-of-env).
  

### Run

  ```bash
  cd geoimg  # (directory with this README file, grandparent of __main__.py file)
  pipenv shell
  python -m geoimg  # This shows available commands, like 'search', 'rgb', etc.
  ```
