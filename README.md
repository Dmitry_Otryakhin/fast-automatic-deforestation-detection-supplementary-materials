# Fast automatic deforestation detectors, supplementary materials
The repository with supplementary code, data and an R package for article "Fast automatic deforestation detectors and their extensions for other spatial objects".


## deforeStableP
It is an R package with implementations of the novel methods of forest detection, and essentially is a custom version of our package deforeStable that was submitted to [CRAN](https://cran.r-project.org/web/packages/deforestable/index.html). File deforeStableP_0.0.9999.tar.gz is a bundled source package and can be installed by running 
```R
remotes::install_local(path="deforeStableP_0.0.9999.tar.gz")
```

## geoimg
It is a python utility that we used to download Sentinel-2 products from PEPS and convert them into RGB images. Geoimg is not a necessary tool for reproduction of the results in this article as we provide all data in the form of RGB images. Though, an interested reader may use it for follow-up research. We refer to geoimg readme file for details of usage. The latest version of the utility can be found in its own [repository](https://gitlab.com/max.rms/geoimg).


## Data and codes
### Deforestation near Arkhangelsk 
Data and code for the practical example from sub-section 1.1 are located in folder Arkhangelsk_deforestation of the supplementary material. R script Training_set_preparation.R was used to cut picture \_3\_2\_.jpeg in sub-frames some of which later were manually selected to create folders forest and non-forest. These folders constitute the training set for the non-parametric model that marked forest and non-forest in image \_2\_4\_.jpeg. This is done by script Arkhangelsk_deforestation.R.

### Exploratory data analysis
Figure 2 from Section 2 is obtained by means of EDA_forest.R (folder EDA) with the help of forest1.jpeg. EDA_forest.R sources EDA.R to load packages and necessary functions. EDA_Sea_Mount.R works in the same fashion and reproduces the results of Appendix B.

### Experiments on images from Occitanie.
The experiments were described in Section 6. Corresponding codes and data can be found in folder France\_experiment of the supplement. Script data\_preparation.R cuts \_4\_4\_.jpeg into sub-frames that are subsequently used in formation of the training-test split, see sub-folders. In all the experiments, work of the frequentist detectors is demonstrated by script Frequentist\_detection.R, while that of machine learning methods---by ML\_detection.py
