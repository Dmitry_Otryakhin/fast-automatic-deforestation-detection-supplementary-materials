#!/usr/bin/env python
# coding: utf-8

# In[18]:


import time
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import learning_curve
import pickle
import csv
import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Conv2D, MaxPool2D, Flatten, Dropout
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam, SGD

from skimage.util import view_as_windows
import skimage
import os
from skimage import io
from skimage.viewer import ImageViewer
import pandas as pd
from sklearn.svm import SVC

# Parameters controlling what to run
# It is necessary to run GENERATE_X_TRAINING_TEST_RESULTS and IMPORT_MDC_AND_SDC_TRAINING_TEST_RESULTS
# before running PLOT_ALL_TEST_RESULTS
GENERATE_SVM_TRAINING_TEST_RESULTS = True
GENERATE_CNN_TRAINING_TEST_RESULTS = True
IMPORT_MDC_AND_SDC_TRAINING_TEST_RESULTS = True
PLOT_ALL_TEST_RESULTS = True
PLOT_SVM_FULL_PICTURE_RESULTS = True
PLOT_CNN_FULL_PICTURE_RESULTS = True

# Training proportions used for GENERATE_X_TRAINING_TEST_RESULTS
PROPORTIONS = np.linspace(0.1,1,46)

# Size of tiles (sub-images)
M = 10
N = 10            


# In[19]:


## SVM classification (with flatten)
def svm_forest(training_proportion):
    # Start time
    start = time.time()
    
    # Training input
    # Forest images
    forest_training_list = []
    forest_training_files = os.listdir('Forest_training/.')
    forest_training_files.sort()
    # Select proportion of forest_training_files
    forest_training_files = forest_training_files[0: int(round(len(forest_training_files)*training_proportion))]
    for file in forest_training_files:
        image = io.imread('Forest_training/' + file)
        forest_training_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]    
        forest_training_list.extend(forest_training_tiles)
    
    # Flatten forest tiles
    flatten_forest_training_tiles = []
    for forest_training_tile in forest_training_list:
        flatten_forest_training_tiles.append(forest_training_tile.flatten() / 255)
    X_training = pd.DataFrame(flatten_forest_training_tiles).dropna()       
        
    number_of_forests_training = len(X_training)

    # Non-forest images
    non_forest_training_list = []
    non_forest_training_files = os.listdir('Non-forest_training/.')
    non_forest_training_files.sort()
    # Select proportion of non_forest_training_files
    non_forest_training_files = non_forest_training_files[0: int(round(len(non_forest_training_files)*training_proportion))]
    for file in non_forest_training_files:
        image = io.imread('Non-forest_training/' + file)
        non_forest_training_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]   
        non_forest_training_list.extend(non_forest_training_tiles)
    
    # Flatten non-forest tiles 
    flatten_non_forest_training_tiles = []
    for non_forest_training_tile in non_forest_training_list:
        flatten_non_forest_training_tiles.append(non_forest_training_tile.flatten() / 255)
    X_training = X_training.append(pd.DataFrame(flatten_non_forest_training_tiles).dropna())

    number_of_non_forests_training  = len(X_training) - number_of_forests_training 

    # Add class (forest or non forest) to df
    X_training.insert(len(X_training.columns), "Class", [1] * number_of_forests_training + [0] * number_of_non_forests_training)

    # Create df with classes
    y_training = pd.DataFrame(X_training.pop("Class"))

    ## Test input
    # Forest images
    forest_testing_list = []
    forest_testing_files = os.listdir('Forest_testing/.')
    forest_testing_files.sort()
    for file in forest_testing_files:
        image = io.imread('Forest_testing/' + file)
        forest_testing_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]    
        forest_testing_list.extend(forest_testing_tiles)

    # Flatten forest tiles
    flatten_forest_testing_tiles = []
    for forest_testing_tile in forest_testing_list:
        flatten_forest_testing_tiles.append(forest_testing_tile.flatten() / 255)
    X_testing = pd.DataFrame(flatten_forest_testing_tiles).dropna()

    number_of_forests_testing = len(X_testing)

    # Non-forest images
    non_forest_testing_list = []
    non_forest_testing_files = os.listdir('Non-forest_testing/.')
    non_forest_testing_files.sort()
    for file in non_forest_testing_files:
        image = io.imread('Non-forest_testing/' + file)
        non_forest_testing_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]   
        non_forest_testing_list.extend(non_forest_testing_tiles)

    # Flatten non-forest tiles
    flatten_non_forest_testing_tiles = []
    for non_forest_testing_tile in non_forest_testing_list:
        flatten_non_forest_testing_tiles.append(non_forest_testing_tile.flatten() / 255)
    X_testing = X_testing.append(pd.DataFrame(flatten_non_forest_testing_tiles).dropna())

    number_of_non_forests_testing  = len(X_testing) - number_of_forests_testing 

    # Add class (forest or non-forest) to df
    X_testing.insert(len(X_testing.columns), "Class", [1] * number_of_forests_testing + [0] * number_of_non_forests_testing)

    # Create df with classes
    y_testing = pd.DataFrame(X_testing.pop("Class"))
    
    # Classification
    # Support vector machine with RBF kernel
    support_vector_machine = SVC(kernel = "rbf").fit(X_training, y_training.values.ravel())
    yp_training = support_vector_machine.predict(X_training)
    yp_testing = support_vector_machine.predict(X_testing)

    training_error_rate = np.mean(np.not_equal(yp_training, y_training.values.ravel()))
    test_error_rate = np.mean(np.not_equal(yp_testing, y_testing.values.ravel()))
    
    end = time.time()
    total_time = end - start
    return(training_error_rate, test_error_rate, total_time)


# In[20]:


## SVM visualization
def svm_forest_visual():
    # Training input
    # Forest images
    forest_training_list = []
    forest_training_files = os.listdir('Forest_training/.')
    forest_training_files.sort()
    for file in forest_training_files:
        image = io.imread('Forest_training/' + file)
        forest_training_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]    
        forest_training_list.extend(forest_training_tiles)
    
    # Flatten forest tiles
    flatten_forest_training_tiles = []
    for forest_training_tile in forest_training_list:
        flatten_forest_training_tiles.append(forest_training_tile.flatten() / 255)
    X_training = pd.DataFrame(flatten_forest_training_tiles).dropna()       
        
    number_of_forests_training = len(X_training)

    # Non-forest images
    non_forest_training_list = []
    non_forest_training_files = os.listdir('Non-forest_training/.')
    non_forest_training_files.sort()
    for file in non_forest_training_files:
        image = io.imread('Non-forest_training/' + file)
        non_forest_training_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]   
        non_forest_training_list.extend(non_forest_training_tiles)
    
    # Flatten non-forest tiles 
    flatten_non_forest_training_tiles = []
    for non_forest_training_tile in non_forest_training_list:
        flatten_non_forest_training_tiles.append(non_forest_training_tile.flatten() / 255)
    X_training = X_training.append(pd.DataFrame(flatten_non_forest_training_tiles).dropna())

    number_of_non_forests_training  = len(X_training) - number_of_forests_training 

    # Add class (forest or non forest) to df
    X_training.insert(len(X_training.columns), "Class", [1] * number_of_forests_training + [0] * number_of_non_forests_training)

    # Create df with classes
    y_training = pd.DataFrame(X_training.pop("Class"))
    
    support_vector_machine = SVC(kernel = "rbf").fit(X_training, y_training.values.ravel())
    
    # Read large test image
    testing_list = []
    test_image = io.imread("_5_4_.jpeg")
    testing_tiles = [test_image[x: x + M, y:y + N] for x in range(0, test_image.shape[0], M) for y in range(0, test_image.shape[1], N)]    
    
    flatten_testing_tiles = []
    for testing_tile in testing_tiles:
        flatten_testing_tiles.append(testing_tile.flatten() / 255)
    X_testing = pd.DataFrame(flatten_testing_tiles).dropna()
    
    yp_testing = support_vector_machine.predict(X_testing)
    
    # Create image from test results
    result_test_image = test_image
    row = 0
    col = 0
    for result in yp_testing:
        if result == 0:
            tile = (np.repeat([np.repeat([[0,0,0]], [10], axis = 0)], [10], axis = 0))
        elif result == 1:
            tile = (np.repeat([np.repeat([[255,255,255]], [10], axis = 0)], [10], axis = 0))
        result_test_image[row:(row+10), col:(col+10)] = tile
        col = col + 10
        if (col >= test_image.shape[0]):
            col = 0
            row = row + 10

    skimage.io.imsave("svm_result_test_image.jpeg", result_test_image)
    skimage.io.imshow(result_test_image)


# In[21]:


## CNN classification (without flatten)
def cnn_forest(training_proportion):
    # Start time
    start = time.time()
    
    # Training input
    # Forest images
    X_training = []
    forest_training_files = os.listdir('Forest_training/.')
    forest_training_files.sort()
    # Select proportion of forest_training_files
    forest_training_files = forest_training_files[0: int(round(len(forest_training_files)*training_proportion))]
    for file in forest_training_files:
        image = io.imread('Forest_training/' + file)
        forest_training_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]    
        X_training.extend(forest_training_tiles)
    
    number_of_forests_training = len(X_training)
    
    # Non forest images
    non_forest_training_files = os.listdir('Non-forest_training/.')
    non_forest_training_files.sort()
    # Select proportion of non_forest_training_files
    non_forest_training_files = non_forest_training_files[0: int(round(len(non_forest_training_files)*training_proportion))]
    for file in non_forest_training_files:
        image = io.imread('Non-forest_training/' + file)
        non_forest_training_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]   
        X_training.extend(non_forest_training_tiles)
    
    number_of_non_forests_training = len(X_training) - number_of_forests_training
    
    y_training = [1] * number_of_forests_training + [0] * number_of_non_forests_training
    
    # Testing input
    # Forest images
    X_testing = []
    forest_testing_files = os.listdir('Forest_testing/.')
    forest_testing_files.sort()
    for file in forest_testing_files:
        image = io.imread('Forest_testing/' + file)
        forest_testing_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]    
        X_testing.extend(forest_testing_tiles)
    
    number_of_forests_testing = len(X_testing)
    
    # Non-forest images
    non_forest_testing_files = os.listdir('Non-forest_testing/.')
    non_forest_testing_files.sort()
    for file in non_forest_testing_files:
        image = io.imread('Non-forest_testing/' + file)
        non_forest_testing_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]   
        X_testing.extend(non_forest_testing_tiles)
    
    number_of_non_forests_testing = len(X_testing) - number_of_forests_testing
    
    y_testing = [1] * number_of_forests_testing + [0] * number_of_non_forests_testing
    
    # Best test accuracy when proportion = 1 and using 10 x 10 images is
    # (2, 8, 3, 0)
    # Different number convolution layers
    NConvolutions = [2] # [1, 2]

    # Different number of filters (neurons in each convolution layer)
    NFilters = [8] # [8, 12, 16]

    # Different number of kernel sizes 
    Kernels = [3] # [2, 3]

    # Different droput percentages
    PDropouts = [0] #[0, 0.05, 0.1]
    
    X_training = np.array(X_training)
    y_training = np.array(y_training)
    X_testing = np.array(X_testing)
    y_testing = np.array(y_testing)
    
    result_cnn = {}
    for NConvolution in NConvolutions:
        for NFilter in NFilters:
            for Kernel in Kernels:
                for PDropout in PDropouts:  
                    inputs_cnn = Input(shape = (M, N, 3))
                    cnn_1 = Conv2D(filters = NFilter, padding = 'same', kernel_size = Kernel, strides = (1, 1), activation = 'relu')(inputs_cnn)
                    mp_1 = MaxPool2D(pool_size = 2)(cnn_1)
                    dr_1 = Dropout(PDropout)(mp_1)

                    if(NConvolution == 2):
                        cnn_2 = Conv2D(filters = NFilter, padding = 'same', kernel_size = Kernel, strides = (1, 1), activation = 'relu')(dr_1)
                        mp_2 = MaxPool2D(pool_size = 2)(cnn_2)
                        dr_2 = Dropout(PDropout)(mp_2)
                        fl = Flatten()(dr_2)
                    else:
                        fl = Flatten()(dr_1)      

                    output_cnn = Dense(1, activation = 'sigmoid')(fl)            

                    model_cnn = Model(inputs_cnn, output_cnn)
                    opt = Adam()
                    model_cnn.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])

                    training_hist_cnn = model_cnn.fit(X_training, y_training, batch_size = 32, epochs = 50, validation_data = (X_testing, y_testing), verbose = 0)
                    result_cnn[(NConvolution, NFilter, Kernel, PDropout)] = (training_hist_cnn, model_cnn.count_params(), training_hist_cnn.history['val_accuracy'][-26], training_hist_cnn.history['val_accuracy'][-1])

                    #print("NConvolution", NConvolution, "NFilter", NFilter, "Kernel", Kernel, "PDropout", PDropout)
                    #print("Number of model parameters", model_cnn.count_params())
                    #print("Middle accuracy", training_hist_cnn.history['val_accuracy'][-26])
                    #print("Final accuracy", training_hist_cnn.history['val_accuracy'][-1])
    
    return(result_cnn)


# In[22]:


## CNN visualization
def cnn_forest_visual(NConvolution, NFilter, Kernel, PDropout):
    # Training input
    # Forest images
    X_training = []
    forest_training_files = os.listdir('Forest_training/.')
    forest_training_files.sort()
    for file in forest_training_files:
        image = io.imread('Forest_training/' + file)
        forest_training_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]    
        X_training.extend(forest_training_tiles)
        
    number_of_forests_training = len(X_training)

    # Non-forest images
    non_forest_training_files = os.listdir('Non-forest_training/.')
    non_forest_training_files.sort()
    for file in non_forest_training_files:
        image = io.imread('Non-forest_training/' + file)
        non_forest_training_tiles = [image[x: x + M, y:y + N] for x in range(0, image.shape[0], M) for y in range(0, image.shape[1], N)]   
        X_training.extend(non_forest_training_tiles)
    
    number_of_non_forests_training  = len(X_training) - number_of_forests_training 

    # Add class (forest or non forest)
    y_training = [1] * number_of_forests_training + [0] * number_of_non_forests_training
    
    # Change type
    X_training = np.array(X_training)
    y_training = np.array(y_training)
    
    # Define CNN
    inputs_cnn = Input(shape = (M, N, 3))
    cnn_1 = Conv2D(filters = NFilter, padding = 'same', kernel_size = Kernel, strides = (1, 1), activation = 'relu')(inputs_cnn)
    mp_1 = MaxPool2D(pool_size = 2)(cnn_1)
    dr_1 = Dropout(PDropout)(mp_1)

    if(NConvolution == 2):
        cnn_2 = Conv2D(filters = NFilter, padding = 'same', kernel_size = Kernel, strides = (1, 1), activation = 'relu')(dr_1)
        mp_2 = MaxPool2D(pool_size = 2)(cnn_2)
        dr_2 = Dropout(PDropout)(mp_2)
        fl = Flatten()(dr_2)
    else:
        fl = Flatten()(dr_1)      

    output_cnn = Dense(1, activation = 'sigmoid')(fl)            

    model_cnn = Model(inputs_cnn, output_cnn)
    opt = Adam()
    model_cnn.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])
    training_hist_cnn = model_cnn.fit(X_training, y_training, batch_size = 32, epochs = 50, verbose = 0)
    
    # Read large test image
    test_image = io.imread("_5_4_.jpeg")
    testing_tiles = [test_image[x: x + M, y:y + N] for x in range(0, test_image.shape[0], M) for y in range(0, test_image.shape[1], N)]    
    X_testing = testing_tiles
    
    # Change type
    X_testing = np.array(X_testing)
    
    # Make prediction
    yp_testing = model_cnn.predict(X_testing)
    
    # Create image from test results
    result_test_image = test_image
    row = 0
    col = 0
    for result in yp_testing:
        if result < 0.5:
            tile = (np.repeat([np.repeat([[0,0,0]], [10], axis = 0)], [10], axis = 0))
        elif result >= 0.5:
            tile = (np.repeat([np.repeat([[255,255,255]], [10], axis = 0)], [10], axis = 0))
        result_test_image[row:(row+10), col:(col+10)] = tile
        col = col + 10
        if (col >= test_image.shape[0]):
            col = 0
            row = row + 10

    skimage.io.imsave("cnn_result_test_image.jpeg", result_test_image)
    skimage.io.imshow(result_test_image)


# In[23]:


# SVM rbf results
if(GENERATE_SVM_TRAINING_TEST_RESULTS):
    print("svm_rbf")
    SVM_rbf_all_training_error_rates = []
    SVM_rbf_all_test_error_rates = []
    for p in PROPORTIONS:
        print(p)
        SVM_rbf = svm_forest(p)
        SVM_rbf_all_training_error_rates.append(SVM_rbf[0])
        SVM_rbf_all_test_error_rates.append(SVM_rbf[1])

    with open('svm_rbf_all_training_error_rates_M' + str(M) + '_N' + str(N) + '.pickle', 'wb') as handle:
        pickle.dump(SVM_rbf_all_training_error_rates, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('svm_rbf_all_test_error_rates_M' + str(M) + '_N' + str(N) + '.pickle', 'wb') as handle:
        pickle.dump(SVM_rbf_all_test_error_rates, handle, protocol=pickle.HIGHEST_PROTOCOL)


# In[24]:


# CNN results
if(GENERATE_CNN_TRAINING_TEST_RESULTS):
    print("CNN")
    cnn_all_training_error_rates = []
    cnn_all_test_error_rates = []
    for p in PROPORTIONS:
        print(p)
        cnn = cnn_forest(p)
        
        best_test_accuracy = 0
        for key in cnn:
            test_accuracy = cnn[key][0].history['val_accuracy'][-1]
            if (test_accuracy > best_test_accuracy):
                best_test_accuracy = test_accuracy
                best_test_accuracy_key = key
                
        # print('best_test_accuracy_key ' + str(best_test_accuracy_key))
        training_accuracy = cnn[best_test_accuracy_key][0].history['accuracy'][-1]

        cnn_all_training_error_rates.append(1 - training_accuracy)
        cnn_all_test_error_rates.append(1 - best_test_accuracy)

    with open('cnn_all_training_error_rates_M' + str(M) + '_N' + str(N) + '.pickle', 'wb') as handle:
        pickle.dump(cnn_all_training_error_rates, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('cnn_all_test_error_rates_M' + str(M) + '_N' + str(N) + '.pickle', 'wb') as handle:
        pickle.dump(cnn_all_test_error_rates, handle, protocol=pickle.HIGHEST_PROTOCOL)


# In[25]:


# Import MDC and SDC results from CSV files
if (IMPORT_MDC_AND_SDC_TRAINING_TEST_RESULTS):
    file = open('err_rates_M' + str(M) + '_N' + str(N) + '.csv')
    csvreader = csv.reader(file)
    header = next(csvreader)
    rows = []
    for row in csvreader:
        rows.append(row)

    proportions = [item[0] for item in rows] 
    test_error_np = [item[1] for item in rows] 
    train_error_np = [item[2] for item in rows] 
    test_error_p = [item[3] for item in rows] 
    train_error_p = [item[4] for item in rows] 

    proportions = [float(item) for item in proportions] 
    test_error_np = [float(item) for item in test_error_np]
    train_error_np = [float(item) for item in train_error_np] 
    test_error_p = [float(item) for item in test_error_p] 
    train_error_p = [float(item) for item in train_error_p]

    with open('non_parametric_training_error_M' + str(M) + '_N' + str(N) + '.pickle', 'wb') as handle:
        pickle.dump(train_error_np, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('parametric_training_error_M' + str(M) + '_N' + str(N) + '.pickle', 'wb') as handle:
        pickle.dump(train_error_p, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('non_parametric_test_error_M' + str(M) + '_N' + str(N) + '.pickle', 'wb') as handle:
        pickle.dump(test_error_np, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('parametric_test_error_M' + str(M) + '_N' + str(N) + '.pickle', 'wb') as handle:
        pickle.dump(test_error_p, handle, protocol=pickle.HIGHEST_PROTOCOL)
    


# In[26]:


# Read and plot all results
if (PLOT_ALL_TEST_RESULTS):
    file_names = ['non_parametric_test_error_M' + str(M) + '_N' + str(N) + '.pickle', 
                  'parametric_test_error_M' + str(M) + '_N' + str(N) + '.pickle',  
                  'svm_rbf_all_test_error_rates_M' + str(M) + '_N' + str(N) + '.pickle', 
                  'cnn_all_test_error_rates_M' + str(M) + '_N' + str(N) + '.pickle']

    training_sizes = PROPORTIONS * 510 * (20 / M) * (20 / N)
    np.round(training_sizes, 0)
    for file_name in file_names:
        print(file_name)
        with open(file_name, 'rb') as handle:
            results = pickle.load(handle)
            if (file_name == 'non_parametric_test_error_M' + str(M) + '_N' + str(N) + '.pickle'):
                label = 'MDC'
            elif (file_name == 'parametric_test_error_M' + str(M) + '_N' + str(N) + '.pickle'):
                label = 'SDC'
            elif (file_name == 'svm_rbf_all_test_error_rates_M' + str(M) + '_N' + str(N) + '.pickle'):
                label = 'SVM'
            elif (file_name == 'cnn_all_test_error_rates_M' + str(M) + '_N' + str(N) + '.pickle'):
                label = 'CNN'
                print(results)
            #print(training_sizes)
            #print(results)
            plt.plot(training_sizes, results, label = label)

    plt.ylabel('Error rate')
    plt.xlabel('Training set size')
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
      fancybox=False, shadow=False, ncol=4)
    plt.ylim([-0.01, 0.20])
    plt.grid()
    plt.savefig('all_results_M' + str(M) + '_N' + str(N) + '.png', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi = 1000)
    plt.show() 


# In[94]:


if (PLOT_SVM_FULL_PICTURE_RESULTS):
    svm_forest_visual()


# In[93]:


if (PLOT_CNN_FULL_PICTURE_RESULTS):
    NConvolution = 2
    NFilter = 8
    Kernel = 3
    PDropout = 0 
    cnn_forest_visual(NConvolution, NFilter, Kernel, PDropout)


# In[ ]:




